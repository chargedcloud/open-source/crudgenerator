<div align="center">

# @chargedcloud/crudgenerator

A package for you to create an API using Express and Sequelize (in relational databases) quickly and easily.

[Installation](#installation) • [Usage](#usage) • [Requests](#requests)

</div>

## Installation

You can install the package using npm:

```sh
npm install @chargedcloud/crudgenerator
```

## Usage

### Implementing services layer

In your services layer of your desired entity, you must import the `serviceGenerator` function. After you import it, you must call it passing the model of your entity as a parameter. The function will return an object with the following properties:

- `findAll`: Function that returns all entities in the database.
- `findOne`: Function that returns an entity in the database by its id.
- `create`: Function that creates a new entity in the database.
- `updateById`: Function that updates an entity in the database by its id.
- `remove`: Function that removes an entity in the database by its id.

```js
import db from "../models/index.mjs";
import createLogger from "@chargedcloud/logger";
import { serviceGenerator } from "@chargedcloud/crudgenerator";

// We recommended to use the createLogger function from @chargedcloud/logger
const log = createLogger(import.meta.url);

// Here you can declare your custom functions
const modelService = {};

const serviceMerged = serviceGenerator({
  modelName: "YourModel",
  dbInstance: db,
  service: modelService,
  multipleDb: false, // If you have multiple databases, put true
  logger: log, // The serviceGenerator function will use the createLogger function from @chargedcloud/logger, otherwise, put null
});

export default serviceMerged;
```

#### Multiple databases

If you have multiple databases, the `serviceGenerator` function will access the models instance using the property `dbName` via `req.user`. So, in your models layer, the `index.mjs` file must return an object with the following structure:

```js
// index.mjs
export const db = {
  database1: {
    YourModel: ...
  },
  database2: {
    YourModel: ...
  }
}
```

After that, in your services layer, you must pass the `dbName` property in the `req.user` object via middleware and `multipleDb` parameter as `true` in the `serviceGenerator` function.

```js
// middleware.mjs
export const middleware = (req, res, next) => {
  req.user = { dbName: "database1" };
  next();
};
```

```js
// services/yourModelService.mjs
import db from "../models/index.mjs";

const serviceMerged = serviceGenerator({
  modelName: "YourModel",
  dbInstance: db,
  service: modelService,
  multipleDb: true,
});
```

**OBSERVATION**: The `dbName` property in the `req.user` object must be the same as the property in the `db` object.

### Implementing controllers layer

Now, in your controllers layer, you must import the `controllerGenerator` function. After you import it, you must call it passing the service of your entity as a parameter. The function will return an object with the same properties of the service.

```js
import { controllerGenerator } from "@chargedcloud/crudgenerator";
import modelService from "../services/yourModelService.mjs";

// Here you can declare your custom functions
const modelController = {};

const controllerMerged = controllerGenerator({
  service: modelService,
  controller: modelController,
});

export default controllerMerged;
```

### Implementing routes layer

Finally, in your routes layer, you must import the `routeGenerator` function. After you import it, you must call it passing the controller of your entity as a parameter. The function will return an object with the routes of your entity.

```js
import { Router } from "express";
import { routeGenerator } from "@chargedcloud/crudgenerator";
import modelController from "../controllers/yourModelController.mjs";

const modelRoutes = Router();

routeGenerator({
  router: modelRoutes,
  controller: modelController,
});

export default modelRoutes;
```

## Requests

In this section, we will show you the different requests that you can make to the API and the responses that you will receive.

### GET /yourModel

This request returns all entities in the database with pagination. The response will be an object with the following properties:

- `count`: Number of entities in the database.
- `rows`: Array with the entities in the database.

```json
{
  "count": 1,
  "rows": [
    {
      "id": 1,
      "name": "John Doe",
      "createdAt": "2021-08-31T18:00:00.000Z",
      "updatedAt": "2021-08-31T18:00:00.000Z"
    }
  ]
}
```

Also, you can pass the following query parameters:

#### page

This parameter is used to indicate the page number of the entities that you want to get. The default value is 1.

#### pageLimit

This parameter is used to indicate the number of entities that you want to get per page. The default value is 20.

#### order

This parameter is used to indicate the order of the entities that you want to get. You can use the order parameter in the following ways:

**OBSEVATION**: We recommend that you JSON.stringify the order parameter in query string.

```js
params: {
  order: JSON.stringify([
    // You can order the fields of the model
    { field: "name", direction: "ASC" },

    // Also, you can order the included models (how many nested levels you want)
    { modelsTarget: ["Comment"], field: "createdAt", direction: "DESC" },
  ]);
}
```

#### where

This parameter is used to indicate the conditions that the entities must meet to be returned. You can use the where parameter in the following ways:

**OBSEVATION**: We recommend that you JSON.stringify the where parameter in query string.

```js
params: {
  where: JSON.stringify({
    // This is a optional parameter. The values are "AND" or "OR"
    operator: "OR",

    conditions: [
      // The default value of operator is eq
      { field: "name", operator: "substring", value: "John" },
      { field: "name", operator: "eq", value: "Doe" },
    ],
  });
}
```

#### include

This parameter is used to indicate the included models that you want to get. You can use the include parameter in the following ways:

**OBSEVATION**: We recommend that you JSON.stringify the include parameter in query string.

```js
params: {
  include: JSON.stringify([
    // Simple include
    { model: "Comment", as: "comments" },

    // Nested include
    {
      model: "Comment",
      as: "comments",
      include: [{ model: "User", as: "user" }],
    },

    // Include with where
    {
      model: "Comment",
      as: "comments",
      where: {
        conditions: [
          { field: "content", operator: "substring", value: "Hello" },
          { field: "content", operator: "substring", value: "World" },
        ],
      },
    },
  ]);
}
```

### GET /yourModel/:id

This request returns an entity in the database by its id. The response will be an object with the entity in the database.

```json
{
  "id": 1,
  "name": "John Doe",
  "createdAt": "2021-08-31T18:00:00.000Z",
  "updatedAt": "2021-08-31T18:00:00.000Z"
}
```

### POST /yourModel

This request creates a new entity in the database. The response will be an object with the entity in the database.

```json
{
  "id": 1,
  "name": "John Doe",
  "createdAt": "2021-08-31T18:00:00.000Z",
  "updatedAt": "2021-08-31T18:00:00.000Z"
}
```

### PATCH /yourModel/:id

This request updates an entity in the database by its id. The response will be an object with the entity in the database.

```json
{
  "id": 1,
  "name": "John Doe",
  "createdAt": "2021-08-31T18:00:00.000Z",
  "updatedAt": "2021-08-31T18:00:00.000Z"
}
```

### DELETE /yourModel/:id

This request removes an entity in the database by its id. The response will be an object with the entity in the database.

```json
{ "id": 1 }
```
