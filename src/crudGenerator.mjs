import createLogger from '@chargedcloud/logger';
import { controllerReturnStatus, defaultFunctions } from './config/consts.mjs';
import InvalidRequestError from './errors/InvalidRequestError.mjs';
import includeGenerator from './functions/includeGenerator.mjs';
import orderGenerator from './functions/orderGenerator.mjs';
import whereGenerator from './functions/whereGenerator.mjs';

/**
  * @param {Object} options
  * @param {object} options.service
  * @param {string} options.modelName
  * @param {object} options.dbInstance
  * @param {function} [options.logger]
  * @param {boolean} [options.multipleDb]
*/

export function serviceGenerator({
  service,
  modelName,
  dbInstance,
  logger,
  multipleDb = false,
}) {
  if (!logger) {
    logger = createLogger(import.meta.url);
  }

  function actualDatabase(request) {
    const dbName = request.user?.dbName;

    if (!multipleDb) return dbInstance;

    if (!dbName) {
      throw new InvalidRequestError('Missing dbName in user object');
    }

    return dbInstance[dbName];
  }

  const log = logger.get('controllergenerator');

  const getById = async (req) => {
    const model = actualDatabase(req)[modelName];

    log.debug(`get by id of table ${model.tableName}`);

    let where = whereGenerator(req.query.where);
    const order = orderGenerator(model.sequelize, req.query.order);
    const include = includeGenerator(model.sequelize, req.query.include);

    where = {
      id: req.params.id,
      ...where,
    };

    const queryOptions = {
      where,
      order,
      include,
    };

    const row = await model.findOne({
      ...queryOptions,
    });

    if (!row) {
      return null;
    }

    return row;
  };

  const serviceFunctions = {
    getById,
    getAll: async (req) => {
      const model = actualDatabase(req)[modelName];

      log.debug(`get table ${model.tableName}`);

      const limit = Number(req.query.pageLimit) || 20;
      const page = Number(req.query.page) || 1;
      const offset = (page - 1) * limit;

      let where = whereGenerator(req.query.where);
      const order = orderGenerator(
        model.sequelize,
        req.query.order,
      );
      const include = includeGenerator(
        model.sequelize,
        req.query.include,
      );

      where = {
        ...where,
      };

      const queryOptions = {
        where,
        limit,
        offset,
        order,
        include,
        distinct: true,
      };

      return model.findAndCountAll({ ...queryOptions });
    },
    insert: async (req) => {
      const model = actualDatabase(req)[modelName];

      log.debug(`insert table ${model.tableName}`);
      delete req.body.id;
      return model.create(req.body);
    },
    updateById: async (req) => {
      const model = actualDatabase(req)[modelName];

      log.debug(`update table ${model.tableName}`);
      const row = await getById(req);

      if (!row) {
        return null;
      }
      delete req.body.id;

      row.set(req.body);
      await row.save();
      return row;
    },
    remove: async (req) => {
      const model = actualDatabase(req)[modelName];

      log.debug(`update table ${model.tableName}`);
      const row = await getById(req);

      if (!row) {
        return null;
      }

      await row.destroy();

      return row.id;
    },
  };

  const newService = { ...service };

  Object.keys(serviceFunctions).forEach((key) => {
    if (defaultFunctions.includes(key)) {
      const autoCrud = serviceFunctions[key];

      if (newService[key]) {
        newService[key] = (req, res, logs) => service[key](req, res, logs, autoCrud);
      } else {
        newService[key] = autoCrud;
      }
    }
  });

  return newService;
}

export function controllerGenerator({ service, controller }) {
  const newController = { ...controller };

  Object.keys(service).forEach((key) => {
    if (defaultFunctions.includes(key)) {
      newController[key] = async (req, res) => {
        const { status, notFoundStatus } = controllerReturnStatus[key];

        const result = await service[key](req);

        if (result === null) {
          return res.status(notFoundStatus).send();
        }

        return res.status(status).json(result);
      };
    }
  });

  return newController;
}

export function routesGenerator({ controller, router }) {
  Object.keys(controller).forEach((key) => {
    if (defaultFunctions.includes(key)) {
      const func = controller[key];

      switch (key) {
        case 'getById':
          router.get('/:id', func);
          break;
        case 'getAll':
          router.get('/', func);
          break;
        case 'insert':
          router.post('/', func);
          break;
        case 'updateById':
          router.patch('/:id', func);
          break;
        case 'remove':
          router.delete('/:id', func);
          break;
        default:
          break;
      }
    }
  });

  return router;
}
