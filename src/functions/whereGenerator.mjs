import { Op } from 'sequelize';

export default function whereGenerator(where) {
  const whereObject = {};

  if (where) {
    if (typeof where === 'string') {
      where = JSON.parse(where);
    }

    const conditions = {};

    where.conditions.forEach((condition) => {
      const isConditionValid = condition.field && condition.operator && condition.value;

      if (!isConditionValid) {
        return;
      }

      if (condition.value === 'null') {
        condition.value = null;
      }

      conditions[`${condition.field}`] = {
        [Op[condition.operator] || Op.eq]: condition.value,
      };
    });

    if (where.operator) {
      whereObject[Op[where.operator]] = conditions;
    } else {
      Object.assign(whereObject, conditions);
    }
  }

  return whereObject;
}
