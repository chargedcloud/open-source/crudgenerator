import whereGenerator from './whereGenerator.mjs';

export default function includeGenerator(sequelize, include) {
  const includeArray = [];

  if (include) {
    if (typeof include === 'string') {
      include = JSON.parse(include);
    }

    include.forEach((includeItem) => {
      const model = sequelize.models[includeItem.model] || null;

      if (!model) throw new Error(`Model ${includeItem.model} not found`);

      const includeObject = { ... includeItem, model };

      if (includeItem.include) {
        includeObject.include = includeGenerator(sequelize, includeItem.include);
      }

      if (includeItem.where) {
        includeObject.where = whereGenerator(includeItem.where);
      }

      includeArray.push(includeObject);
    });
  }

  return includeArray;
}
