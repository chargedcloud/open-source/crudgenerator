export default function orderGenerator(sequelize, order) {
  if (typeof order === 'string') {
    order = JSON.parse(order);
  }

  if (!order || order.length === 0) {
    return [];
  }

  const orderArray = [];

  order.forEach((orderItem) => {
    const { modelsTarget } = orderItem;

    if (modelsTarget) {
      const models = modelsTarget.map((modelTarget) => {
        const model = sequelize.models[modelTarget] || null;

        if (!model) throw new Error(`Model ${modelTarget} not found`);

        return model;
      });

      orderArray.push([...models, orderItem.field, orderItem.direction]);
    } else {
      orderArray.push([orderItem.field, orderItem.direction]);
    }
  });

  return orderArray;
}
