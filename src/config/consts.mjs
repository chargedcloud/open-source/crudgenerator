/* eslint-disable import/prefer-default-export */
export const defaultFunctions = [
  'getAll', 'getById', 'insert', 'updateById', 'remove',
];

export const controllerReturnStatus = {
  getAll: { status: 200 },
  getById: { status: 200, notFoundStatus: 404 },
  insert: { status: 201 },
  updateById: { status: 200, notFoundStatus: 404 },
  remove: { status: 200, notFoundStatus: 404 },
};
